package ar.edu.unju.fi.tp02.controladores;

import java.util.ArrayList;
import java.util.List;

import ar.edu.unju.fi.tp02.dominio.OrdenDePago;

public class GestorPago {
	
	
	public static List<OrdenDePago> generarPagos() {
		int j = 100;
		List<OrdenDePago> lista = new ArrayList<OrdenDePago>();
		lista.add(new OrdenDePago(1, j + 3, "1", "2", 1.0, 1, 1));
		lista.add(new OrdenDePago(2, j + 1, "2", "2", 2.0, 2, 2));
		lista.add(new OrdenDePago(3,j + 2, "3", "3", 3.0, 2, 3));
		return lista;

	}
	
	public static void mostrar(OrdenDePago a) {
		System.out.println(a);
	}
	public static void mostrarPagos(List<OrdenDePago> listaDePagos) {
		for (OrdenDePago a : listaDePagos) {
			mostrar(a);
		}
	}

	
	public static OrdenDePago buscarPago(List<OrdenDePago> listaDePagos, Integer nDeOP) {
		OrdenDePago pago = null;
		for (OrdenDePago a : listaDePagos) {
			if (a.getNumeroOP().intValue() == nDeOP.intValue())
				pago = a;
		}
		return pago;
	}
	
	

	public static void buscarBeneficiario(List<OrdenDePago> listaDePagos, String nDeBeneficiario) {
		for (OrdenDePago a : listaDePagos) {
			if (a.getCubBeneficiario() == nDeBeneficiario)
				mostrar(a);
		}

	}

	public static void mostrarPagosEnDolares(List<OrdenDePago> listaDePagos) {
		double contador = 0;
		for (OrdenDePago a : listaDePagos) {
			if (a.getMoneda().intValue() == 2) {
				mostrar(a);
				contador += a.getImporte();
			}
		}
		System.out.println("total de valor en contador= " + contador);
	}

	
}
