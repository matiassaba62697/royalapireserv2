package ar.edu.unju.fi.tp02.controladores;

import java.util.Map;
import java.util.TreeMap;

import ar.edu.unju.fi.tp02.dominio.OrdenDePago;

public class GestorPagoTreeMap {
	public static Map generarPagos() {
		int j = 100;
		TreeMap lista = new TreeMap();
		OrdenDePago pago1=new OrdenDePago(1, j + 1, "1", "2", 1.0, 1, 1);
		OrdenDePago pago2=new OrdenDePago(2, j + 2, "2", "2", 2.0, 2, 2);
		OrdenDePago pago3=new OrdenDePago(3, j + 3, "3", "3", 3.0, 2, 3);
		lista.put(pago1.getNumeroOP(), pago1);
		lista.put(pago2.getNumeroOP(), pago2);
		lista.put(pago3.getNumeroOP(), pago3);
		return lista;
	}

	public static void mostrar(Object a) {
		System.out.println(a);
	}

	public static void mostrarPagos(Map listaDePagos) {
		for (int i=101;i<=103;i=i+1) {
			OrdenDePago pago=(OrdenDePago)listaDePagos.get(i); 
			mostrar(pago);
		}
	}

	public static OrdenDePago buscarOperacion(Map listaDePagos, Integer nDeOP) {
		OrdenDePago pago=null;
		for (int i=101;i<=103;i++) {
			pago=(OrdenDePago)listaDePagos.get(i);
			if (pago.getNumeroOP() == nDeOP.intValue())
			return pago;
		}
		return null;
	}

	public static void buscarBeneficiario(Map listaDePagos, String nDeBeneficiario) {
		for (int i=101;i<=103;i++) {
			OrdenDePago pago=(OrdenDePago)listaDePagos.get(i);
			if (pago.getCubBeneficiario() == nDeBeneficiario)
				mostrar(pago);
		}
	}

	public static void mostrarPagosEnDolares(Map listaDePagos) {
		double contador = 0;
		for (int i=101;i<=103;i++) {
			OrdenDePago pago=(OrdenDePago)listaDePagos.get(i);
			if (pago.getMoneda() == 2) {
				mostrar(pago);
				contador += pago.getImporte();
			}
		}
		mostrar("total de valor en contador= " + contador);
	}


}
