package ar.edu.unju.fi.tp02.dominio;

import java.util.Date;

import ar.edu.unju.fi.tp02.util.Util;

public class OrdenDePago implements Comparable<OrdenDePago> {

	private Integer id;
	private Integer numeroOP;
	private String cubOrdenante;
	private String cubBeneficiario;
	private Double importe;
	private Integer moneda;
	private Integer facturaAsociada;
	Date fecha = new Date();

	public OrdenDePago() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {

		return "N� OP=" + Util.FormatoOperacion(numeroOP) + numeroOP.toString() + " -CBU Ordenante=" + cubOrdenante
				+ " -CBU Beneficiario=" + cubBeneficiario + " -Fecha de Emicion=" + Util.FormatoFecha(fecha)
				+ " -Importe=" + importe + " -Moneda=" + Util.FormatoMoneda(moneda) + " -Factura Asociada="
				+ facturaAsociada;
	}

	public OrdenDePago(Integer id, Integer numeroOP, String cubOrdenante, String cubBeneficiario, Double importe,Integer moneda, Integer facturaAsociada) {
		super();
		this.id = id; this.numeroOP = numeroOP; this.cubOrdenante = cubOrdenante; this.cubBeneficiario = cubBeneficiario;
		this.importe = importe;
		this.moneda = moneda;
		this.facturaAsociada = facturaAsociada;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNumeroOP() {
		return numeroOP;
	}

	public void setNumeroOP(Integer numeroOP) {
		this.numeroOP = numeroOP;
	}

	public String getCubOrdenante() {
		return cubOrdenante;
	}

	public void setCubOrdenante(String cubOrdenante) {
		this.cubOrdenante = cubOrdenante;
	}

	public String getCubBeneficiario() {
		return cubBeneficiario;
	}

	public void setCubBeneficiario(String cubBeneficiario) {
		this.cubBeneficiario = cubBeneficiario;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public Integer getMoneda() {
		return moneda;
	}

	public void setMoneda(Integer moneda) {
		this.moneda = moneda;
	}

	public Integer getFacturaAsociada() {
		return facturaAsociada;
	}

	public void setFacturaAsociada(Integer facturaAsociada) {
		this.facturaAsociada = facturaAsociada;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Override
	public int compareTo(OrdenDePago object) {
		if(numeroOP < object.getNumeroOP())
		return -1;
			else if(numeroOP > object.getNumeroOP())
				return 1;
		return 0;
	}

}
