package ar.edu.unju.fi.tp02.test;

import java.util.Collections;
import java.util.List;

import ar.edu.unju.fi.tp02.controladores.GestorPago;
import ar.edu.unju.fi.tp02.dominio.OrdenDePago;

public class TestOrdenDePago {


	public static void main(String[] args) {
		System.out.println("mostar todos los pagos");
		List<OrdenDePago> listaDePagos = GestorPago.generarPagos();
		Collections.sort(listaDePagos);
		GestorPago.mostrarPagos(listaDePagos);
		// --------------------------------------------------------------------
		System.out.println("pagos en dolares..");
		GestorPago.mostrarPagosEnDolares(listaDePagos);
		// --------------------------------------------------------------------

		System.out.println("busqueda De Beneficiario");
		GestorPago.buscarBeneficiario(listaDePagos, "2");
		// --------------------------------------------------------------------
		System.out.println("buscar Por Numero De Operacion");
		OrdenDePago buscado = GestorPago.buscarPago(listaDePagos, 20);
		if (buscado != null) {
			System.out.println("Se encontro Operacion");
			System.out.println(buscado);
		} else
			System.out.println("no existe Operacion");
	}

}
