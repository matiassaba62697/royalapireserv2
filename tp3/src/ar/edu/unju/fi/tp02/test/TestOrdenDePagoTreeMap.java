package ar.edu.unju.fi.tp02.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import ar.edu.unju.fi.tp02.controladores.GestorPagoTreeMap;
import ar.edu.unju.fi.tp02.dominio.OrdenDePago;

public class TestOrdenDePagoTreeMap {


	public static void main(String[] args) {
		System.out.println("mostar todos los pagos");
		Map listaDePagos = GestorPagoTreeMap.generarPagos();
		GestorPagoTreeMap.mostrarPagos(listaDePagos);
		//------------------------------------------------------------------
		System.out.println("busqueda De Beneficiario");
		GestorPagoTreeMap.buscarBeneficiario(listaDePagos, "2");
		// --------------------------------------------------------------------
		System.out.println("pagos en dolares..");
		GestorPagoTreeMap.mostrarPagosEnDolares(listaDePagos);
		// --------------------------------------------------------------------
				System.out.println("buscar Por Numero De Operacion");
				OrdenDePago buscado = GestorPagoTreeMap.buscarOperacion(listaDePagos, 101);
				if (buscado != null) {
					System.out.println("Se encontro Operacion");
					System.out.println(buscado);
				} else
					System.out.println("no existe Operacion");
	}

}
