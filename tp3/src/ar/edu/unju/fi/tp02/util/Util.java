package ar.edu.unju.fi.tp02.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Util {

	public static String FormatoFecha(Date fecha) {
		SimpleDateFormat formato = new SimpleDateFormat("dd'/'MM'/'yyyy", new Locale("es", "AR"));
		String fechaFormateada = formato.format(fecha);
		return fechaFormateada;

	}

	public static String FormatoOperacion(Integer id) {
		String id2 = "0";
		for (int i = id.toString().length(); i <= 9; i++) {
			id2 = id2 + "0";
		}
		return id2;
	}

	public static String FormatoMoneda(int moneda) {
		String tipoDeMoneda = "Peso";
		if (moneda == 2) {
			tipoDeMoneda = "Dolar";
		}
		return tipoDeMoneda;
	}

}
